<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>


# Lemonade timer

Install packages through:

```
npm i
```

Run service through: 
```
npm run start
```

To run the service on debug mode (with debug logs of fired postbacks):

```
npm run start:dev
```

An example request to add new timer:
```
POST localhost:3000/timers

BODY:
{
    "hours": 0,
    "minutes": 0,
    "seconds": 3,
    "url": "https://www.google.com"
}
```

An example request to get a timer by id:

```
GET localhost:3000/timers/a4ab374b-deda-4658-baba-41ebe1d01655
```