import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { SetTimerDto } from './dto/set-timer.dto';
import { TimerService } from './timer.service';

@Controller('')
export class TimerController {
  constructor(private readonly timerService: TimerService) {}

  @Get('timers/:id')
  async getTimer(@Param('id') id: string): Promise<object> {
    return await this.timerService.getTimerSecondsLeftById(id);
  }

  @Post('timers')
  async setTime(@Body() setTimerDto: SetTimerDto): Promise<object> {
    return await this.timerService.setNewTimer(setTimerDto);
  }
}
