import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Timer } from './entities/timer.entity';
import { TimerController } from './timer.controller';
import { TimerService } from './timer.service';

@Module({
  imports: [TypeOrmModule.forFeature([Timer]), HttpModule],
  controllers: [TimerController],
  providers: [TimerService],
})
export class TimerModule {}
