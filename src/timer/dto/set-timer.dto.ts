import { IsNumber, IsUrl, Min } from 'class-validator';

export class SetTimerDto {
  @IsNumber()
  @Min(0)
  readonly hours: number;

  @IsNumber()
  @Min(0)
  readonly minutes: number;

  @IsNumber()
  @Min(0)
  readonly seconds: number;

  @IsUrl()
  readonly url: string;
}
