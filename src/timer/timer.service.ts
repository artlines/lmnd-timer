import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import * as dayjs from 'dayjs';
import { SetTimerDto } from './dto/set-timer.dto';
import * as duration from 'dayjs/plugin/duration';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Timer } from './entities/timer.entity';
import { Cron } from '@nestjs/schedule';
import { HttpService } from '@nestjs/axios';

dayjs.extend(duration);

@Injectable()
export class TimerService {
  constructor(
    @InjectRepository(Timer)
    private timersRepository: Repository<Timer>,
    private httpService: HttpService,
  ) {}
  private readonly logger = new Logger('logger-test');

  async setNewTimer(setTimerDto: SetTimerDto): Promise<object> {
    try {
      const timerTargetTimestamp = dayjs()
        .add(
          dayjs.duration({
            hours: setTimerDto.hours,
            minutes: setTimerDto.minutes,
            seconds: setTimerDto.seconds,
          }),
        )
        .unix();

      const timer = new Timer();
      timer.timestamp = timerTargetTimestamp;
      timer.url = setTimerDto.url;

      const timerInserted = await this.timersRepository.save(timer);
      return { id: timerInserted.id };
    } catch (e) {
      // emit log
      throw e;
    }
  }

  async getTimerSecondsLeftById(id: string): Promise<object> {
    try {
      const timer = await this.timersRepository.findOne({ where: { id } });
      if (!timer) {
        throw new NotFoundException('Timer with specified id was not found');
      }

      const secsLeft = timer.timestamp - dayjs().unix();

      return { id: timer.id, secondsLeft: secsLeft > 0 ? secsLeft : 0 };
    } catch (e) {
      // emit log
      throw e;
    }
  }

  @Cron('* * * * * *')
  async firePostbackJob() {
    const timers = await this.timersRepository.find({
      where: { isActivated: false },
    });
    const currentTimestamp = dayjs().unix();

    for (const timer of timers) {
      if (timer.timestamp <= currentTimestamp) {
        await this.httpService.post(timer.url, {});
        this.logger.debug(`Fired postback to url ${timer.url}`);

        // update cron
        timer.isActivated = true;
        await this.timersRepository.update(timer.id, timer);
      }
    }
  }
}
