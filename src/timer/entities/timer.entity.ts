import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Timer {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  timestamp: number;

  @Column()
  url: string;

  @Column('boolean', { default: false })
  isActivated: boolean;
}
