import { Module } from '@nestjs/common';
import { TimerModule } from './timer/timer.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    TimerModule,
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'database.db',
      synchronize: true,
      logging: false,
      autoLoadEntities: true,
    }),
    ScheduleModule.forRoot(),
  ],
})
export class AppModule {}
